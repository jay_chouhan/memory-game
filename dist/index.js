const header = document.getElementById('header');
const bar = document.getElementById('bar');
const gameGrid = document.getElementById('game');
const overlay = document.getElementById('overlay');
const start = document.getElementById('start');
const movesId = document.getElementById('moves');
const timerId = document.getElementById('timer');
const highscoreElementstart = document.getElementById('highscore');
const alertMsg = document.getElementById('alert');

const cards = [
  'fas fa-atom',
  'fas fa-atom',
  'fab fa-galactic-senate',
  'fab fa-galactic-senate',
  'fas fa-meteor',
  'fas fa-meteor',
  'fas fa-robot',
  'fas fa-robot',
  'fas fa-satellite',
  'fas fa-satellite',
  'fas fa-rocket',
  'fas fa-rocket',
  'fas fa-space-shuttle',
  'fas fa-space-shuttle',
  'fas fa-user-astronaut',
  'fas fa-user-astronaut',
];

let displayedCards = [];
let moves = 0;
let matchPair = 0;
let timer = 0;
let score = 0;
let highscore = localStorage.getItem('highscore');
let interval;

if (highscore) {
  highscoreElementstart.innerText = `highscore: ${highscore}`;
}

const createCard = (card) => {
  const div = document.createElement('div');
  div.classList.add('card');
  div.setAttribute('data-type', card);

  const i = document.createElement('i');
  i.className += card;

  div.appendChild(i);

  return div;
};

const shuffleCards = (cards) => {
  cards.forEach((card, index) => {
    const rndIndex = Math.floor(Math.random() * cards.length);
    const temp = cards[index];
    cards[index] = cards[rndIndex];
    cards[rndIndex] = temp;
  });

  return cards;
};

const createResetButton = () => {
  const resetDiv = document.createElement('div');
  resetDiv.className += 'resetDiv';

  const resetbtn = document.createElement('i');

  resetbtn.className = 'fas fa-redo-alt';

  if (matchPair === 8) {
    resetbtn.addEventListener('click', restart);
  } else {
    resetbtn.addEventListener('click', resetConfirm);
  }

  resetDiv.appendChild(resetbtn);

  return resetDiv;
};

const countMoves = () => {
  moves += 1;
  movesId.innerText = `moves: ${moves}`;
};

const setTimer = () => {
  interval = setInterval(() => {
    timer += 1;
    timerId.innerText = `time(sec): ${timer}`;
  }, 1000);
};

const setActive = (card) => {
  card.classList.add('active');
  card.classList.add('disabled');
  displayedCards.push(card);
  countMoves();
};

const calcScore = () => {
  score = Math.ceil((1 / moves) * (1 / timer) * 100000) * 100;
};

const checkHighScore = () => {
  if (!localStorage.getItem('highscore') || score > highscore) {
    highscore = score;
    localStorage.setItem('highscore', highscore);
  }
};

const checkWin = () => {
  if (matchPair === 8) {
    overlay.style.display = 'flex';
    overlay.style.background = 'rgba(0,0,0,0.7)';

    const win = document.createElement('h1');
    win.innerText = 'YOU WIN!!!';

    calcScore();

    const scoreElement = document.createElement('h2');
    scoreElement.innerText = `Score: ${score}`;

    checkHighScore();

    const highscoreElementEnd = document.createElement('h2');
    highscoreElementEnd.innerText = `Highscore: ${highscore}`;

    gameGrid.innerHTML = '';

    overlay.innerHTML = '';

    clearInterval(interval);

    bar.lastChild.remove();

    overlay.append(win);
    overlay.append(scoreElement);
    overlay.append(highscoreElementEnd);
    overlay.append(createResetButton());
  }
};

const matchCards = () => {
  if (displayedCards.length === 2) {
    if (displayedCards[0].getAttribute('data-type') === displayedCards[1].getAttribute('data-type')) {
      matchPair += 1;
      displayedCards = [];
      checkWin();
    } else {
      gameGrid.style.pointerEvents = 'none';
      setTimeout(() => {
        displayedCards[0].classList.remove('active');
        displayedCards[0].classList.remove('disabled');

        displayedCards[1].classList.remove('active');
        displayedCards[1].classList.remove('disabled');
        displayedCards = [];
        gameGrid.style.pointerEvents = 'auto';
      }, 500);
    }
  }
};

const startGame = () => {
  overlay.innerHTML = '';
  gameGrid.style.display = 'grid';

  header.style.display = 'flex';
  bar.appendChild(createResetButton());

  const shuffled = shuffleCards(cards);
  setTimer();

  let shuffledCards = shuffled.map((card) => createCard(card));
  shuffledCards.forEach((card) => {
    gameGrid.appendChild(card);
    card.addEventListener('click', (event) => {
      setActive(card);
      matchCards();
    });
  });
};

start.addEventListener('click', () => {
  overlay.style.display = 'none';
  startGame();
});

const restart = () => {
  displayedCards = [];
  moves = 0;
  matchPair = 0;
  timer = 0;
  score = 0;
  gameGrid.innerHTML = '';

  const resetDestroy = document.getElementsByClassName('resetDiv')[0];
  resetDestroy.remove();

  clearInterval(interval);
  movesId.innerText = 'moves: 0';
  timerId.innerText = 'time(sec): 0';

  startGame();
};

const resetConfirm = () => {
  const confirmDiv = document.createElement('div');
  confirmDiv.setAttribute('id', 'confirmDiv');

  const yes = document.createElement('i');
  yes.className += 'fas fa-check';

  const no = document.createElement('i');
  no.className += 'fas fa-times';

  const h2 = document.createElement('h2');
  h2.innerText = 'Are You Sure?';

  header.style.display = 'none';
  overlay.style.display = 'none';
  gameGrid.style.display = 'none';

  yes.addEventListener('click', () => {
    restart();
    confirmDiv.remove();
  });

  no.addEventListener('click', () => {
    confirmDiv.remove();

    overlay.style.display = 'flex';
    header.style.display = 'flex';
    gameGrid.style.display = 'grid';
  });

  alertMsg.append(confirmDiv);
  confirmDiv.append(h2);
  confirmDiv.append(yes);
  confirmDiv.append(no);
};
